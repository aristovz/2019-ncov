//
//  Pallete.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

enum Pallete {
    enum Common {
        enum NavigationBar {
            static let tintColor = UIColor(hexString: "25282B")
        }
        
        enum TabBar {
            static let tintColor = UIColor(hexString: "FFA500")
        }
    }
    
    enum StatisticsController {
        enum ChartView {
            static let diedLineColor = UIColor(hexString: "FF6969")
            static let curedLineColor = UIColor(hexString: "59CD95")
            
            static let axisLineColor = UIColor(hexString: "638394")
            static let gridLineColor = UIColor(hexString: "D3E1E9")
        }
    }
}
