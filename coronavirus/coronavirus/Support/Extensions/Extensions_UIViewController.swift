//
//  Extensions_UIViewController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import WebKit
import StoreKit
import MessageUI
import CoreLocation

// MARK: - Open rate controller

extension UIViewController {
    func openRateController() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + Global.appID) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

// MARK: - Open share controller

extension UIViewController {
    func openShareAppController() {
        guard let appUrl = URL(string: Global.appLink) else { return }
        openShareController(activityItems: [appUrl])
    }
    
    func openShareController(activityItems: [Any], applicationActivities: [UIActivity]? = nil) {
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}

// MARK: - Call to phone number

extension UIViewController {
    func call(to phone: String) {
        openLink("tel://\(phone)")
    }

    func call(to phones: [String]) {
        
        let alertController = UIAlertController(title: "Выберите телефон", message: nil, preferredStyle: .actionSheet)
        
        for phone in phones {
            let callAction = UIAlertAction(title: phone, style: .default) { _ in
                self.call(to: phone)
            }
            
            alertController.addAction(callAction)
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Open link in Safari from out of app

extension UIViewController {
    func openLink(_ url: URL) {
        guard UIApplication.shared.canOpenURL(url) else { return }
        
        UIApplication.shared.open(url)
    }
    
    func openLink(_ link: String) {
        guard let url = URL(string: link) else { return }
        
        openLink(url)
    }
    
    func openLinkInApp(_ link: String) {
        
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else { return }
        
        let viewController = UIViewController()
        let webView = WKWebView()
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        viewController.view.addSubview(webView)
        
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: viewController.view.topAnchor),
            webView.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor),
            webView.leadingAnchor.constraint(equalTo: viewController.view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: viewController.view.trailingAnchor)
        ])
        
        let request = URLRequest(url: url)
        webView.load(request)
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Open modal mail app in send message state

extension UIViewController: MFMailComposeViewControllerDelegate {
    func openMailController(_ recipients: [String] = []) {
        
        guard MFMailComposeViewController.canSendMail() else { return }
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients(recipients)
        
        present(mail, animated: true)
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true)
    }
}

// MARK: - Start refresh controll programatically

extension UIRefreshControl {
    func beginRefreshingManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
        beginRefreshing()
    }
}

// MARK: - Show alert with dialog for open navigator to location

extension UIViewController {
    
    enum NavigatorApp {
        case yandexMap, yandexNavi, google, apple
        
        var alertTitle: String {
            switch self {
            case .yandexMap: return "Яндекс.Карты"
            case .yandexNavi: return "Яндекс.Навигатор"
            case .google: return "Google Maps"
            case .apple: return "Apple Maps"
            }
        }
        
        func getScheme(for location: CLLocation) -> String {
            
            let coordinate = location.coordinate
            
            switch self {
            case .yandexMap: return "yandexmaps://maps.yandex.ru/?rtext=~\(coordinate.latitude),\(coordinate.longitude)"
            case .yandexNavi: return "yandexnavi://build_route_on_map?lat_to=\(coordinate.latitude)&lon_to=\(coordinate.longitude)"
            case .google: return "comgooglemaps://?daddr=\(coordinate.latitude),\(coordinate.longitude)"
            case .apple: return "http://maps.apple.com/?daddr=\(coordinate.latitude),\(coordinate.longitude)"
            }
        }
    }
    
    func selectNavigatorDialog(location: CLLocation, in navigators: [NavigatorApp] = .all) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for navigator in navigators {
            alert.addAction(UIAlertAction(title: navigator.alertTitle, style: .default, handler: { _ in
                self.openLink(navigator.getScheme(for: location))
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension Array where Element == UIViewController.NavigatorApp {
    static var all: [Element] {
        return yandex + [.google, .apple]
    }
    
    static var yandex: [Element] {
        return [.yandexNavi, .yandexMap]
    }
}
