//
//  Extension_UIFont.swift
//  FitnesApp
//
//  Created by Pavel Aristov on 18.01.19.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

extension UIFont {
    enum Roboto: String {
        case regular = "Roboto-Regular"
        case medium = "Roboto-Medium"
        case bold = "Roboto-Bold"
    }
    
    class func getRobotoFont(font: Roboto = .medium, size: CGFloat = 15) -> UIFont {
        return UIFont(name: font.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func printFonts() {
        UIFont.familyNames.forEach({
            print("\n--- ", $0, " ---")
            print(UIFont.fontNames(forFamilyName: $0))
        })
    }
}
