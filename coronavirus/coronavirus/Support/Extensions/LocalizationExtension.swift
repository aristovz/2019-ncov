//
//  LocalizationExtension.swift
//  LearningEnglishApp
//
//  Created by Pavel Aristov on 09.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    public var localized: String {
        return self.localizedWithComment("")
    }
    
    public func localizedWithComment(_ comment: String) -> String {
        if let string = self.localizedWithComment(comment, bundle: Bundle.main, recursion: 1) {
            return string
        }
        
        return self
    }
    
    fileprivate func localizedWithComment(_ comment: String, bundles: [Bundle]) -> String? {
        for bundle in bundles {
            if bundle != Bundle.main {
                if let string = self.localizedWithComment(comment, bundle: bundle, recursion: 1) {
                    return string
                }
            }
        }
        
        return nil
    }
    
    fileprivate func localizedWithComment(_ comment: String, bundle: Bundle, recursion: Int) -> String? {
        let string = NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: comment)
        
        if self != string {
            return string
        }
        
        if recursion > 0 {
            if let urls = bundle.urls(forResourcesWithExtension: "bundle", subdirectory: nil) {
                for subURL in urls {
                    if let subBundle = Bundle(url: subURL) {
                        if let subString = self.localizedWithComment(comment, bundle: subBundle, recursion: recursion - 1) {
                            return subString
                        }
                    }
                }
                
            }
        }
        
        return nil;
    }
    
    func getLzText(original: String? = nil) -> String {
        
        var result = self.localized
        
        if result == self {
            self.writeEmptyLocalizationKey(original: original)
            result = original ?? ""
        }
        
        return result
    }
    
    private func writeEmptyLocalizationKey(original: String? = nil, isJSON: Bool = false) {
        
        #if RELEASE
        return
        #endif
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent("EmptyLocalizationKeys.txt")
            
            do {
                let fileUpdater = try FileHandle(forUpdating: fileURL)
                
                let content = String(data: fileUpdater.readDataToEndOfFile(), encoding: .utf8)
                guard !(content?.contains(self) ?? false) else { return }
                
                fileUpdater.seekToEndOfFile()
                
                var stringToWrite = "\"\(self)\" = \"\(original ?? "EMPTY_KEY")\";\n"
                if isJSON { stringToWrite = "\"\(self)\": \"\(original ?? "EMPTY_KEY")\",\n" }
                
                fileUpdater.write(stringToWrite.data(using: .utf8)!)
                fileUpdater.closeFile()
                print("[Localization] Success write empty key:", self)
            }
            catch {//} let error {
//                print("[Localization] Error while write key:", self, error.localizedDescription, fileURL)
            }
        }
    }
    
    static func printEmptyLocalizationKeysPath() {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("EmptyLocalizationKeys.txt")
            print(fileURL.absoluteString.replacingOccurrences(of: "file://", with: ""))
        }
    }
}

extension UILabel {
    
    @IBInspectable public var lzText: String? {
        set {
            self.text = newValue?.getLzText(original: self.text)
        }
        
        get {
            return self.text
        }
    }
    
    @IBInspectable public var lzAttributedText: String? {
        set {
            if let localized = newValue?.getLzText(original: self.text) {
                let attributes = self.attributedText?.attributes(at: 0, effectiveRange: nil)
                self.attributedText = NSAttributedString(string: localized, attributes: attributes)
            } else {
                self.attributedText = nil
            }
        }
        
        get {
            return self.attributedText?.string
        }
    }
}

extension UITextField {
    
    @IBInspectable public var lzText: String? {
        set {
            if newValue != nil {
                self.text = newValue?.getLzText(original: self.text)
            }
            else {
                self.text = nil
            }
        }
        
        get {
            return self.text
        }
    }
    
    @IBInspectable public var lzPlaceholder: String? {
        set {
            if newValue != nil {
                self.placeholder = newValue?.getLzText(original: self.placeholder)
            }
            else {
                self.placeholder = nil
            }
        }
        
        get {
            return self.placeholder
        }
    }
}

extension UIButton {
    
    @IBInspectable public var lzTitle: String? {
        set { setLocalizedTitle(newValue, state: UIControl.State.normal) }
        get { return getTitleForState(UIControl.State.normal) }
    }
    
    @IBInspectable public var lzAttributedText: String? {
        set { setLocalizedAttributedTitle(newValue, state: UIControl.State.normal) }
        get { return getTitleForState(UIControl.State.normal) }
    }
    
//    @IBInspectable public var lzTitleHighlighted: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.highlighted) }
//        get { return getTitleForState(UIControl.State.highlighted) }
//    }
//
//    @IBInspectable public var lzTitleDisabled: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.disabled) }
//        get { return getTitleForState(UIControl.State.disabled) }
//    }
//
//    @IBInspectable public var lzTitleSelected: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.selected) }
//        get { return getTitleForState(UIControl.State.selected) }
//    }
//
//    @IBInspectable public var lzTitleFocused: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.focused) }
//        get { return getTitleForState(UIControl.State.focused) }
//    }
//
//    @IBInspectable public var lzTitleApplication: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.application) }
//        get { return getTitleForState(UIControl.State.application) }
//    }
//
//    @IBInspectable public var lzTitleReserved: String? {
//        set { setLocalizedTitle(newValue, state: UIControl.State.reserved) }
//        get { return getTitleForState(UIControl.State.reserved) }
//    }
    
    fileprivate func setLocalizedTitle(_ title: String?, state: UIControl.State) {
        let currentTitle = getTitleForState(state)
        self.setTitle(title?.getLzText(original: currentTitle), for: state)
    }
    
    fileprivate func setLocalizedAttributedTitle(_ title: String?, state: UIControl.State) {
        self.setTitle(title?.localized, for: state)
    }
    
    fileprivate func getTitleForState(_ state: UIControl.State) -> String? {
        if let title = self.titleLabel {
            return title.text
        }
        return nil
    }
}

extension UIBarItem {
    
    @IBInspectable public var lzTitle: String? {
        set { self.title = newValue?.getLzText(original: self.title) }
        get { return self.title }
    }
}

extension UINavigationItem {
    
    @IBInspectable public var lzTitle: String? {
        set { self.title = newValue?.getLzText(original: self.title) }
        get { return self.title }
    }
    
    @IBInspectable public var lzPrompt: String? {
        set { self.prompt = newValue?.getLzText(original: self.prompt) }
        get { return self.prompt }
    }
}

extension UIViewController {
    
    @IBInspectable public var lzTitle: String? {
        set { self.title = newValue?.getLzText(original: self.title) }
        get { return self.title }
    }
}

extension NavigationBarView {
    @IBInspectable public var lzTitle: String? {
        set { self.title = newValue?.getLzText(original: self.title) ?? "" }
        get { return self.title }
    }
    
    @IBInspectable public var lzSubTitle: String? {
        set { self.subtitle = newValue?.getLzText(original: self.subtitle) }
        get { return self.subtitle }
    }
}
