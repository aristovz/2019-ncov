//
//  Extension_NSLayoutConstraint.swift
//  Wallpapers
//
//  Created by Pavel Aristov on 19/03/2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UIStackView {
    
    @IBInspectable var spacing_SE: CGFloat {
        get { return self.spacing }
        set {
            if UIScreen.main.bounds.size.height < 650 {
                self.spacing = newValue
            }
        }
    }
    
}

extension NSLayoutConstraint {
    
    // All variation of Screen Height for iPhone
    // 5s, SE - 568 | 6, 6S, 7, 8 - 667 | 6, 6S, 7, 8 Plus - 736 | X, XS, 11 Pro - 812 | XR, XS, 11, 11 Pro Max - 896;
    
    // height: 568
    @IBInspectable var s_5: CGFloat {
        get { return self.constant }
        set {
            if UIScreen.main.bounds.size.height < 650 {
                self.constant = newValue
            }
        }
    }
    
    // height: 667
    @IBInspectable var s_8: CGFloat {
        get { return self.constant }
        set {
            if UIScreen.main.bounds.size.height > 660 && UIScreen.main.bounds.size.height < 670 {
                self.constant = newValue
            }
        }
    }
    
    // height: 736
    @IBInspectable var s_8plus: CGFloat {
        get { return self.constant }
        set {
            if UIScreen.main.bounds.size.height > 670 && UIScreen.main.bounds.size.height < 750 {
                self.constant = newValue
            }
        }
    }
    
    // height: 812
    @IBInspectable var s_x: CGFloat {
        get { return self.constant }
        set {
            if UIScreen.main.bounds.height > 810 && UIScreen.main.bounds.height < 850 {
                self.constant = newValue
            }
        }
    }
    
    // height: 896
    @IBInspectable var s_xr_xsmax: CGFloat {
        get { return self.constant }
        set {
            if UIScreen.main.bounds.height > 890 {
                self.constant = newValue
            }
        }
    }
    
    @IBInspectable var s_iPad: CGFloat {
        get { return self.constant }
        set {
            if UIDevice.current.userInterfaceIdiom == .pad {
                self.constant = newValue
            }
        }
    }
}

