//
//  AppInfo.swift
//  MajorAutoApp
//
//  Created by Pavel Aristov on 23.01.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import Foundation

struct AppInfo {
    
    private static var infoDictionary: [String: Any] {
        return Bundle.main.infoDictionary!
    }
    
    static let version = infoDictionary["CFBundleShortVersionString"] as! String
    static let build = infoDictionary["CFBundleVersion"] as! String
    
}

public extension UIDevice {
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
}
