//
//  Global.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import RealmSwift
import Foundation

class Global {
    
    static let appID = "1499240930"
    static let appLink = "https://apps.apple.com/ru/app/id\(appID)"
    
    static let webSiteLink = "https://coronavirus-monitor.ru/"
    static let supportEmail = "contact@coronavirus-monitor.ru"
    
    static let locale = Locale(identifier: Locale.current.regionCode ?? "en")
}

extension Global {
    static var overallStatistics: OverallStatistics? {
        get {
            print("[Global] Get overallStatistics")
            
            let realm = try! Realm()
            let overallStatistics = realm.objects(OverallStatistics.self).last
            return overallStatistics
        }
        set {
            guard let newValue = newValue else { return }
            
            print("[Global] Set overallStatistics")
            
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
                realm.add(newValue)
            }
        }
    }
    
    static var countriesStatistics: [BaseCity] {
        get {
            print("[Global] Get countriesStatistics")
            
            let realm = try! Realm()
            let countriesStatistics = realm.objects(BaseCity.self)
            return Array(countriesStatistics)
        }
        set {
            print("[Global] Set countriesStatistics")
            
            let realm = try! Realm()
            try! realm.write {
                for country in newValue {
                    realm.create(BaseCity.self, value: country.updatedMap, update: .modified)
                }
            }
        }
    }
}

extension Global {
    static var pushToken: String {
        get {
            return UserDefaults.standard.value(forKey: "pushToken") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "pushToken")
        }
    }
    
    static var fcmPushToken: String {
        get {
            return UserDefaults.standard.value(forKey: "fcmPushToken") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "fcmPushToken")
        }
    }
}
