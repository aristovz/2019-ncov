//
//  OverallStatistics.swift
//  coronavirus
//
//  Created by Pavel Aristov on 10.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import RealmSwift
import Foundation

class OverallStatistics: Object {
    
    @objc dynamic var totalConfirmed: Int = -1
    @objc dynamic var totalDeaths: Int = -1
    @objc dynamic var totalCured: Int = -1
    
    var cities = List<City>()
    var statisticsDays = List<StatisticsDay>()
    
    static func map(from map: [String: Any]) -> OverallStatistics? {
        guard let totalDeaths = map["totalDeaths"] as? Int,
                let totalConfirmed = map["totalConfirmed"] as? Int,
                let totalCured = map["cured"] as? Int,
                let citiesMap = map["cities"] as? [[String: Any]],
                let statisticsDaysMap = map["statistics"] as? [[String: Any]] else {
            print("Can't init OverallStatistics")
            return nil
        }
        
        let overallStatistics = OverallStatistics()
        
        overallStatistics.totalDeaths = totalDeaths
        overallStatistics.totalConfirmed = totalConfirmed
        overallStatistics.totalCured = totalCured
        
        let cities = List<City>()
        citiesMap.forEach {
            if let city = City.map(from: $0) {
                cities.append(city)
            }
        }
        
        overallStatistics.cities = cities
        
        let statisticsDays = List<StatisticsDay>()
        statisticsDaysMap.forEach {
            if let statisticsDay = StatisticsDay.map(from: $0) {
                statisticsDays.append(statisticsDay)
            }
        }
        overallStatistics.statisticsDays = statisticsDays
        
        return overallStatistics
    }
}
