//
//  City.swift
//  coronavirus
//
//  Created by Pavel Aristov on 10.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import RealmSwift
import Foundation

class BaseCity: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    
    @objc dynamic var confirmed: Int = -1
    @objc dynamic var deaths: Int = -1
    var cured = RealmOptional<Int>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func baseMap(from map: [String: Any]) -> BaseCity? {
        guard let confirmed = map["confirmed"] as? Int,
                let deaths = map["deaths"] as? Int else {
            print("Can't init BaseCity")
            return nil
        }
        
        let baseCity = BaseCity()
        
        baseCity.id = baseCity.incrementID()
        baseCity.confirmed = confirmed
        baseCity.deaths = deaths
        
        baseCity.cured = RealmOptional(map["cured"] as? Int)
        
        if let name = map[Global.locale.identifier] as? String {
            baseCity.name = name
        } else if let name = map["ru"] as? String {
            baseCity.name = name
        } else if let name = map["en"] as? String {
            baseCity.name = name
        } else if let name = map["ch"] as? String {
            baseCity.name = name
        } else {
            baseCity.name = "---"
        }
        
        return baseCity
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(BaseCity.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    var updatedMap: [String: Any] {
        return [
            "confirmed": confirmed,
            "deaths": deaths,
            "cured": cured
        ]
    }
}

class City: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    
    @objc dynamic var confirmed: Int = -1
    @objc dynamic var deaths: Int = -1
    var cured = RealmOptional<Int>()
    
    @objc dynamic var coordinates: Coordinate!
    
    static func map(from map: [String: Any]) -> City? {
        guard let id = map["_id"] as? String,
                let confirmed = map["confirmed"] as? Int,
                let deaths = map["deaths"] as? Int,
                let coordinates = map["coordinates"] as? [Double], coordinates.count > 1 else {
            print("Can't init City")
            return nil
        }
        
        let city = City()
        
        city.id = id
        
        city.confirmed = confirmed
        city.deaths = deaths
        
        city.cured = RealmOptional(map["cured"] as? Int)
        
        city.coordinates = Coordinate.map(latitude: coordinates[0], longitude: coordinates[1])
        
        if let name = map["ru"] as? String {
            city.name = name
        } else if let name = map["en"] as? String {
            city.name = name
        } else if let name = map["ch"] as? String {
            city.name = name
        } else {
            city.name = "---"
        }
        
        return city
    }
}

class Coordinate: Object {
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    
    static func map(latitude: Double, longitude: Double) -> Coordinate {
        let coordinate = Coordinate()
        coordinate.latitude = latitude
        coordinate.longitude = longitude
        return coordinate
    }
}
