//
//  StatisticsDay.swift
//  coronavirus
//
//  Created by Pavel Aristov on 10.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import RealmSwift
import Foundation

class StatisticsDay: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var date: Date = Date()
    
    @objc dynamic var confirmedCh: Int = -1
    @objc dynamic var curedCh: Int = -1
    @objc dynamic var diedCh: Int = -1
    
    @objc dynamic var confirmedOther: Int = -1
    @objc dynamic var curedOther: Int = -1
    
    var diedOther = RealmOptional<Int>()
 
    private static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // 2020-02-09T00:00:00.000Z
        return formatter
    }
    
    static func map(from map: [String: Any]) -> StatisticsDay? {
        guard let id = map["_id"] as? String,
                let dateString = map["date"] as? String,
                let date = dateFormatter.date(from: dateString),
                let confirmedCh = map["confirmedCh"] as? Int,
                let curedCh = map["curedCh"] as? Int,
                let diedCh = map["diedCh"] as? Int,
                let confirmedOther = map["confirmedOther"] as? Int,
                let curedOther = map["curedOther"] as? Int else {
            print("Can't init StatisticsDay")
            return nil
        }
        
        let statisticsDay = StatisticsDay()
        
        statisticsDay.id = id
        statisticsDay.date = date
        statisticsDay.confirmedCh = confirmedCh
        statisticsDay.curedCh = curedCh
        statisticsDay.diedCh = diedCh
        statisticsDay.confirmedOther = confirmedOther
        statisticsDay.curedOther = curedOther
        
        statisticsDay.diedOther = RealmOptional(map["diedOther"] as? Int)
     
        return statisticsDay
    }
    
}
