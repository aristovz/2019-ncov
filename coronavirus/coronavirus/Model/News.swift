//
//  News.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation

struct News: Codable {
    let id: String
    let title: String
    let source: String
    let url: URL
    let date: String
    
    let preview: URL?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title
        case source
        case url
        case date
        case preview
    }
}
