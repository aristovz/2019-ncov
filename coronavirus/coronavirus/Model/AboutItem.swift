//
//  AboutItem.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import Foundation

enum AboutItem {
    
    case feedback, webSite, rate, share
    
    var title: String {
        switch self {
        case .feedback: return "about_feedback".getLzText(original: "Обратная связь")
        case .webSite: return "about_webSite".getLzText(original: "Перейти на сайт")
        case .rate: return "about_rate".getLzText(original: "Оценить приложение")
        case .share: return "about_share".getLzText(original: "Поделиться приложением")
        }
    }
    
    var icon: UIImage {
        switch self {
        case .feedback: return #imageLiteral(resourceName: "about_feedback")
        case .webSite: return #imageLiteral(resourceName: "about_webSite")
        case .rate: return #imageLiteral(resourceName: "about_rate")
        case .share: return #imageLiteral(resourceName: "about_share")
        }
    }
    
    static func getLocalData() -> [AboutItem] {
        return [feedback, webSite, rate, share]
    }
}
