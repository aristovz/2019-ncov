//
//  AppDelegate.swift
//  coronavirus
//
//  Created by Pavel Aristov on 09.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import YandexMapKit

import Firebase
import FirebaseFirestore
import FirebaseMessaging

let db = Firestore.firestore()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appRouter: Router?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupLibraries()
        
        let mainTabBarController = MainTabBarController()
        let navigationController = UINavigationController(rootViewController: mainTabBarController)
    
        appRouter = Router(navigationController, in: &window)
        
        return true
    }

}

extension AppDelegate {
    private func setupLibraries() {
        // Yandex MapKit
        YMKMapKit.setApiKey(Constants.mapKitApiKey)
        
        // Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Analytics.setAnalyticsCollectionEnabled(false)
        
        String.printEmptyLocalizationKeysPath()
    }
}
