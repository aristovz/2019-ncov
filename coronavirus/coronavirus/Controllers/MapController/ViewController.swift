//
//  ViewController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import Foundation
import YandexMapKit

class NavigationController: UINavigationController {
    
}

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Test"
        
//        mainTabBarController.mainTabBarController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        mainTabBarController.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @IBAction func sliderValueChange(_ sender: UISlider) {
        print(sender.value)
        
//        mainTabBarController.navigationController?.navigationBar.backgroundColor = UIColor.green.withAlphaComponent(CGFloat(sender.value))
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.green.withAlphaComponent(CGFloat(sender.value))
    }
    
}
