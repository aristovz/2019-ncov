//
//  MapController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import YandexMapKit

class MapController: UIViewController {

    private var mapView: YMKMapView = {
        let mapView = YMKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    
    private let FONT_SIZE: CGFloat = 15
    private let MARGIN_SIZE: CGFloat = 3
    private let STROKE_SIZE: CGFloat = 1
    
    private var points = [YMKPoint: Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupUI()
    }
    
    private func setupUI() {
        
        self.view.backgroundColor = .white
        
        addMapView()
    }
    
    private func addMapView() {
        
        addPoints()
        
        mapView.mapWindow.map.mapObjects.addTapListener(with: self)
        
        self.view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mapView.topAnchor.constraint(equalTo: self.view.topAnchor),
            mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor)
        ])
    }

    private func addPoints() {
        guard let cities = Global.overallStatistics?.cities else {
            updateStatistics()
            return
        }
        
        Array(cities).forEach {
            let point = YMKPoint(latitude: $0.coordinates.latitude, longitude: $0.coordinates.longitude)
            self.mapView.mapWindow.map.mapObjects.addPlacemark(with: point, image: generateImage($0.confirmed), style: YMKIconStyle())
            self.points[point] = $0.confirmed
        }
    }
    
    private func updateStatistics() {
        API.updateStatistics { _, error in
            guard error == nil else { return }
            self.addPoints()
        }
    }
    
    func generateImage(_ number: Int) -> UIImage {
        let scale = UIScreen.main.scale
        let text = "\(number)"
        let font = UIFont.getRobotoFont(size: FONT_SIZE * scale)
        let size = text.size(withAttributes: [.font: font])
        let textRadius = sqrt(size.height * size.height + size.width * size.width) / 2
        let internalRadius = textRadius + MARGIN_SIZE * scale
        let externalRadius = internalRadius + STROKE_SIZE * scale
        let iconSize = CGSize(width: externalRadius * 2, height: externalRadius * 2)

        UIGraphicsBeginImageContext(iconSize)
        let ctx = UIGraphicsGetCurrentContext()!

        ctx.setFillColor(UIColor.white.cgColor)
        ctx.fillEllipse(in: CGRect(
            origin: .zero,
            size: CGSize(width: 2 * externalRadius, height: 2 * externalRadius)));

        ctx.setFillColor(Pallete.Common.TabBar.tintColor.cgColor)
        ctx.fillEllipse(in: CGRect(
            origin: CGPoint(x: externalRadius - internalRadius, y: externalRadius - internalRadius),
            size: CGSize(width: 2 * internalRadius, height: 2 * internalRadius)));

        (text as NSString).draw(
            in: CGRect(
                origin: CGPoint(x: externalRadius - size.width / 2, y: externalRadius - size.height / 2),
                size: size),
            withAttributes: [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: UIColor.white])
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        return image
    }
}

extension MapController: YMKMapObjectTapListener {
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        
        guard let confirmed = points[point] else { return false }
        
        let alert = UIAlertController(
            title: "Tap",
            message: String(format: "Tapped cluster with %u items", confirmed),
            preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: "OK", style: .default, handler: nil))

        present(alert, animated: true, completion: nil)

        return true
    }
}
