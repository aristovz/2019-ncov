//
//  NewsCell.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import SDWebImage

class NewsCell: UITableViewCell {
    
    @IBOutlet private weak var previewShadowView: UIView!
    @IBOutlet private weak var previewImageView: UIImageView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var sourceLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    @IBOutlet private weak var separatorView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        previewShadowView.dropShadow(color: .black, opacity: 0.18, offSet: CGSize(width: 0, height: 2), radius: 17)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        UIView.animate(withDuration: 0.15) {
            self.previewImageView.transform = highlighted ? CGAffineTransform(scaleX: 0.97, y: 0.97) : .identity
            self.previewShadowView.transform = highlighted ? CGAffineTransform(scaleX: 0.97, y: 0.97) : .identity
        }
    }
    
    func setupUI(_ news: News, isLastCell: Bool) {
        
        self.previewImageView.sd_setImage(with: news.preview)
        
        self.titleLabel.text = news.title
        self.sourceLabel.text = news.source
        self.dateLabel.text = news.date
        
        self.separatorView.isHidden = isLastCell
    }
}
