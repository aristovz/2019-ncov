//
//  NewsController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class NewsController: UIViewController {

    // MARK: - UI Elements
    
    private var tableView: UITableView = {
        let tableView = UITableView()
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        
        tableView.clipsToBounds = false
        tableView.contentInset.top = -44
        tableView.scrollIndicatorInsets.top = -44
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.register(NewsCell.self)
        
        return tableView
    }()
    
    private var tableViewHeaderView: NavigationBarView = {
        let headerView = NavigationBarView()
        
        headerView.title = "news_navigation_title".getLzText(original: "Новости")
        headerView.subtitle = "news_navigation_subtitle".getLzText(original: "Важные новости")
        
        headerView.autoresizingMask = []
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        return headerView
    }()
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: - Local variables
    
    private var news: [News] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        refreshControl.beginRefreshingManually()
        refreshData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNavigationBarAlpha(scrollView: tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateNavigationBarAlpha(scrollView: tableView)
    }
}

// MARK: - Add UI Elements

extension NewsController {
    private func setupUI() {
        
        self.view.backgroundColor = .white
        
        addTableView()
    }
    
    private func addTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.refreshControl = refreshControl
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor)
        ])
        
        tableView.tableHeaderView = tableViewHeaderView
        
        NSLayoutConstraint.activate([
            tableViewHeaderView.topAnchor.constraint(equalTo: tableView.topAnchor),
            tableViewHeaderView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            tableViewHeaderView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
        ])

        tableViewHeaderView.layoutIfNeeded()
    }
}

// MARK: - Work with data methods

extension NewsController {
    @objc private func refreshData() {
        API.getNews { news, error in
            self.refreshControl.endRefreshing()
            self.news = news
        }
    }
}


// MARK: - UITableView methods

extension NewsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as NewsCell
        cell.setupUI(news[indexPath.row], isLastCell: indexPath.row == news.count - 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let currentNews = news[indexPath.row]
        let browserController = BrowserController(url: currentNews.url)
        self.tabBarController?.navigationController?.pushViewController(browserController, animated: true)
        
//        openLink(currentNews.url)
    }
}

extension NewsController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        (tabBarController as? UIScrollViewDelegate)?.scrollViewDidScroll?(scrollView)
    }
}
