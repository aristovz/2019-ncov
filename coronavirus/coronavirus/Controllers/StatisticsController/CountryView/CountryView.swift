//
//  CountryView.swift
//  coronavirus
//
//  Created by Pavel Aristov on 10.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class CountryView: NibView {

    @IBOutlet private weak var colorIndicatorView: UIView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    
    @IBOutlet private weak var separatorView: UIView!
    
    convenience init(_ baseCity: BaseCity/*, percent: Double*/, isLastCell: Bool) {
        self.init()
        
//        colorIndicatorView.backgroundColor = .
        
        titleLabel.text = baseCity.name
        subtitleLabel.text = "\(baseCity.confirmed)"//" (\(String(format: "%.2f", percent))%)"
        
        separatorView.isHidden = isLastCell
    }
}
