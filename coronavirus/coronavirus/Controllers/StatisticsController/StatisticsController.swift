//
//  StatisticsController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import Charts
import MBProgressHUD

class StatisticsController: UIViewController {

    @IBOutlet private weak var scrollView: UIScrollView!
    
    @IBOutlet private weak var totalConfirmedLabel: UILabel!
    @IBOutlet private weak var totalDeathsLabel: UILabel!
    @IBOutlet private weak var totalCuredLabel: UILabel!
    
    @IBOutlet private weak var datesIntervalLabel: UILabel!
    @IBOutlet private weak var chartView: LineChartView!
    
    @IBOutlet private weak var countriesStackView: UIStackView!
    
    @IBOutlet private var shadowViews: [UIView]!
    @IBOutlet private var countriesUI: [UIView]!
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refreshControl
    }()
    
    private var overallStatistics: OverallStatistics! {
        didSet {
            updateUI()
        }
    }
    
    private var countriesStatistics: [BaseCity] = [] {
        didSet {
            updateCountriesUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshData()
        dropShadow()
        updateNavigationBarAlpha(scrollView: scrollView)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateNavigationBarAlpha(scrollView: scrollView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.dropShadow()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        dropShadow()
    }
    
    @objc private func refreshData(isRefreshControl: Bool = false) {
        
        if isRefreshControl {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        API.updateStatistics { overallStatistics, error in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.refreshControl.endRefreshing()
            
            guard let overallStatistics = overallStatistics, error == nil else {
                return
            }
            
            self.overallStatistics = overallStatistics
        }
        
        API.getCountriesStats { countriesStats, error in
            guard error == nil else { return }
            
            self.countriesStatistics = !countriesStats.isEmpty ? countriesStats : Global.countriesStatistics
        }
    }

    private func setupUI() {
        scrollView.refreshControl = refreshControl
        
        scrollView.contentInset.top = -44
        scrollView.scrollIndicatorInsets.top = -44
        scrollView.delegate = tabBarController as? UIScrollViewDelegate
    }
    
    private func dropShadow() {
        shadowViews.forEach {
            $0.dropShadow(color: .black, opacity: 0.08, offSet: CGSize(width: 0, height: 2), radius: 25)
        }
    }
    
    private func updateUI() {
        
        guard let lastStatisticDay = overallStatistics.statisticsDays.last else { return }
        
        totalConfirmedLabel.text = "\(lastStatisticDay.confirmedOther + lastStatisticDay.confirmedCh)"
        totalDeathsLabel.text = "\((lastStatisticDay.diedOther.value ?? 0) + lastStatisticDay.diedCh)"
        totalCuredLabel.text = "\(lastStatisticDay.curedOther + lastStatisticDay.curedCh)"
        
        updateChart()
    }
    
    private func updateCountriesUI() {
        
        UIView.animate(withDuration: 0.15, animations: {
            self.countriesUI.forEach { $0.alpha = self.countriesStatistics.isEmpty ? 0.0 : 1.0 }
        }, completion: { _ in
            self.countriesUI.forEach { $0.isHidden = self.countriesStatistics.isEmpty }
        })
            
        guard !countriesStatistics.isEmpty else { return }
    
        countriesStackView.subviews.forEach { $0.removeFromSuperview() }
        
        for k in 0..<countriesStatistics.count {
            let country = countriesStatistics[k]
//            let percent = Double(country.confirmed) / Double(overallStatistics.totalConfirmed)
            let countryView = CountryView(country/*, percent: percent*/, isLastCell: k == countriesStatistics.count - 1)
            
            countriesStackView.addArrangedSubview(countryView)
        }
    }
    
}

// MARK: - Setup Chart UI

extension StatisticsController {
    private func updateChart() {
        
        let days = overallStatistics.statisticsDays
        
        if let dateStart = days.first?.date, let dateEnd = days.last?.date {
        
            let daysDateFormatter = DateFormatter()
            daysDateFormatter.locale = Global.locale
            daysDateFormatter.dateFormat = "dd MMMM"
            
            let yearDateFormatter = DateFormatter()
            yearDateFormatter.dateFormat = "YYYY"

            datesIntervalLabel.text = daysDateFormatter.string(from: dateStart) + " - " + daysDateFormatter.string(from: dateEnd) + ", " + yearDateFormatter.string(from: dateEnd)
        }
        
        
        var diedLineChartEntry = [ChartDataEntry]()
        var curedLineChartEntry = [ChartDataEntry]()
        
        for k in 0..<days.count {
            let curedDataPoint = days[k].curedCh
            let curedEntry = ChartDataEntry(x: Double(k), y: Double(curedDataPoint))
            curedLineChartEntry.append(curedEntry)
            
            let diedDataPoint = days[k].diedCh
            let diedEntry = ChartDataEntry(x: Double(k), y: Double(diedDataPoint))
            diedLineChartEntry.append(diedEntry)
        }
        
        let diedLineDataSet = setupLineDataSet(entries: diedLineChartEntry, color: Pallete.StatisticsController.ChartView.diedLineColor)
        let curedLineDataSet = setupLineDataSet(entries: curedLineChartEntry, color: Pallete.StatisticsController.ChartView.curedLineColor)
        
        setupChartViewUI(dataSets: [diedLineDataSet, curedLineDataSet])
    }
    
    private func setupLineDataSet(entries: [ChartDataEntry], color: UIColor) -> LineChartDataSet {
        let dataSet = LineChartDataSet(entries: entries)

        dataSet.lineWidth = 2
        dataSet.lineWidth = 2
        dataSet.circleRadius = 5
        dataSet.circleHoleRadius = 1
        
        dataSet.circleColors = [color]
        
//        dataSet.drawValuesEnabled = false
        
        dataSet.setColor(color)
        
        return dataSet
    }
    
    private func setupChartViewUI(dataSets: [LineChartDataSet]) {
        
        let data = LineChartData(dataSets: dataSets)
        data.highlightEnabled = false
        
        chartView.data = data
        
        chartView.drawBordersEnabled = false
        chartView.borderColor = .lightGray
        chartView.borderLineWidth = 1
        
        chartView.legend.enabled = false
        chartView.setVisibleXRangeMaximum(7)
        
        chartView.rightAxis.enabled = false
        
        chartView.xAxis.yOffset = 10
        chartView.xAxis.axisMaximum = Double(dataSets[0].entries.count) - 0.9
        chartView.xAxis.granularity = 1
        chartView.xAxis.valueFormatter = self
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.granularityEnabled = true
        
        chartView.xAxis.labelTextColor = Pallete.StatisticsController.ChartView.axisLineColor
        chartView.xAxis.axisLineColor = Pallete.StatisticsController.ChartView.axisLineColor
        chartView.xAxis.gridColor = Pallete.StatisticsController.ChartView.gridLineColor
        
        
        chartView.leftAxis.xOffset = 10
        chartView.leftAxis.axisMinimum = 0
//        chartView.leftAxis.granularity = 1000
//        chartView.leftAxis.granularityEnabled = true
//        chartView.leftAxis.drawGridLinesEnabled = false
        
        chartView.leftAxis.labelTextColor = Pallete.StatisticsController.ChartView.axisLineColor
        chartView.leftAxis.axisLineColor = Pallete.StatisticsController.ChartView.axisLineColor
        chartView.leftAxis.gridColor = Pallete.StatisticsController.ChartView.gridLineColor
        
        guard !dataSets.isEmpty else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.chartView.moveViewToX(Double(dataSets[0].entries.count - 1))
        }
    }
}

extension StatisticsController: IAxisValueFormatter {
    
    private var chartDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM"
        return formatter
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let xIndex = Int(value)

        guard xIndex >= 0 && xIndex < overallStatistics.statisticsDays.count else { return "" }
        
        let day = overallStatistics.statisticsDays[xIndex]
        return chartDateFormatter.string(from: day.date)
    }
}
