//
//  MainTabBarController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    private let mapControllerIndex = 2
    private let navigationBarTitles = [
        "statistics_navigation_title".getLzText(original: "Статистика"),
        "information_navigation_title".getLzText(original: "Информация"),
        "Карта",
        "news_navigation_title".getLzText(original: "Новости"),
        "about_navigation_title".getLzText(original: "О приложении")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupDefaultViewControllers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(selectedIndex == mapControllerIndex, animated: false)
    }
    
    private func setupUI() {
        
        tabBar.isTranslucent = false
        tabBar.barTintColor = .white
        
        tabBar.tintColor = Pallete.Common.TabBar.tintColor
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    private func setupDefaultViewControllers() {
        
        let statisticsItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "tabbar1"), tag: 0)
        let informationItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "tabbar2"), tag: 1)
        let mapItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "tabbar3"), tag: mapControllerIndex)
        let newsItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "tabbar4"), tag: 3)
        let aboutItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "tabbar5"), tag: 4)
        
        let statisticsController = StatisticsController.xibViewController()
        statisticsController.tabBarItem = statisticsItem
        
        let informationController = InformationController()
        informationController.tabBarItem = informationItem
        
        let mapController = MapController()
        mapController.tabBarItem = mapItem
        
        let newsController = NewsController()
        newsController.tabBarItem = newsItem
        
        let aboutController = AboutController()
        aboutController.tabBarItem = aboutItem
            
        self.setViewControllers([
            statisticsController,
            informationController,
            mapController,
            newsController,
            aboutController
        ], animated: false)
        
        fixTabbarItemsInsets()
        
        self.selectedIndex = mapControllerIndex
    }
    
    private func fixTabbarItemsInsets() {

        var offset: CGFloat = UIDevice.current.hasTopNotch ? 6.0 : 0.0

        if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
            offset = 0.0
        }

        if let items = tabBar.items {
            for item in items {
                item.imageInsets = UIEdgeInsets(top: offset, left: 0, bottom: -offset, right: 0)
            }
        }
    }
}

extension MainTabBarController {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        title = navigationBarTitles[item.tag]
        navigationController?.setNavigationBarHidden(item.tag == mapControllerIndex, animated: false)
        
        if item.tag == 0 || item.tag == 3 {
            NotificationsService.shared.registerForPushNotifications(force: true)
        }
    }
}

extension MainTabBarController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = min(1, (scrollView.contentOffset.y - 44) / 20)
        navigationController?.navigationBar.alpha = max(0.001, offset)
    }
}

extension UIViewController {
    func updateNavigationBarAlpha(scrollView: UIScrollView) {
        (tabBarController as? UIScrollViewDelegate)?.scrollViewDidScroll?(scrollView)
    }
}
