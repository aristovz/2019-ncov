//
//  AboutItemCell.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class AboutItemCell: UITableViewCell {

    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        containerView.dropShadow(color: .black, opacity: 0.06, offSet: CGSize(width: 0, height: 2), radius: 25)
        iconImageView.dropShadow(color: .black, opacity: 0.21, offSet: CGSize(width: 0, height: 5), radius: 16)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        UIView.animate(withDuration: 0.15) {
            self.containerView.transform = highlighted ? CGAffineTransform(scaleX: 0.97, y: 0.97) : .identity
        }
    }
    
    func setupUI(_ item: AboutItem) {
        self.titleLabel.text = item.title
        self.iconImageView.image = item.icon
    }
}
