//
//  AboutController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import MessageUI

class AboutController: UIViewController {

    // MARK: - UI Elements
    
    private var tableView: UITableView = {
        let tableView = UITableView()
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.rowHeight = 74
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.clipsToBounds = false
        tableView.contentInset.top = -44
        tableView.contentInset.bottom = 20
        tableView.scrollIndicatorInsets.top = -44
        
        tableView.register(AboutItemCell.self)
        
        return tableView
    }()
    
    private var tableViewHeaderView: AboutHeaderView = {
        let headerView = AboutHeaderView()
        headerView.autoresizingMask = []
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    // MARK: - Local variables
    
    private let aboutItems = AboutItem.getLocalData()
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNavigationBarAlpha(scrollView: tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateNavigationBarAlpha(scrollView: tableView)
    }
}

// MARK: - Add UI Elements

extension AboutController {
    private func setupUI() {
        
        self.view.backgroundColor = .white
        
        addTableView()
    }
    
    private func addTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor)
        ])
        
        self.tableView.tableHeaderView = tableViewHeaderView
        
        NSLayoutConstraint.activate([
            tableViewHeaderView.heightAnchor.constraint(equalToConstant: 226),
            tableViewHeaderView.topAnchor.constraint(equalTo: tableView.topAnchor),
            tableViewHeaderView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            tableViewHeaderView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
        ])

        tableViewHeaderView.layoutIfNeeded()
    }
}

// MARK: - UITableView methods

extension AboutController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aboutItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AboutItemCell
        cell.setupUI(aboutItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = aboutItems[indexPath.row]
        
        switch item {
        case .feedback: openMailController([Global.supportEmail])
        case .webSite: openLink(Global.webSiteLink) // openLinkInApp(Global.webSiteLink)
        case .rate: openRateController()
        case .share: openShareAppController()
        }
    }
}

extension AboutController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateNavigationBarAlpha(scrollView: tableView)
    }
}
