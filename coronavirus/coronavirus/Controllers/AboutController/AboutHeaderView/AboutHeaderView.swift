//
//  AboutHeaderView.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class AboutHeaderView: NibView {

    @IBOutlet private weak var appIconShadowView: UIView!
    @IBOutlet private weak var appIconImageView: UIImageView!
    
    @IBOutlet private weak var versionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        appIconShadowView.dropShadow(color: .black, opacity: 0.08, offSet: CGSize(width: 0, height: 2), radius: 25)
    }
    
    private func setupUI() {
        versionLabel.text = "common_version".getLzText(original: "Версия") + " " + AppInfo.version
    }
}
