//
//  InformationController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class InformationController: UIViewController {
    
    @IBOutlet private var scrollView: UIScrollView!
    
    @IBOutlet private var shadowViews: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentInset.top = -44
        scrollView.scrollIndicatorInsets.top = -44
        scrollView.delegate = tabBarController as? UIScrollViewDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNavigationBarAlpha(scrollView: scrollView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setShadows()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setShadows()
        updateNavigationBarAlpha(scrollView: scrollView)
    }
    
    private func setShadows() {
        shadowViews.forEach {
            $0.dropShadow(color: .black, opacity: 0.08, offSet: CGSize(width: 0, height: 2), radius: 25)
        }
    }
}
