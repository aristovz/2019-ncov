//
//  Router.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

class Router {
    
    private let navigationController: UINavigationController
    
    init(_ navigationController: UINavigationController, in window: inout UIWindow?) {
        
        self.navigationController = navigationController
        
        self.navigationController.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "backIcon")
        self.navigationController.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "backIcon")
        self.navigationController.navigationBar.tintColor = Pallete.Common.NavigationBar.tintColor
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
}
