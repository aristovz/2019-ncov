//
//  BrowserController.swift
//  coronavirus
//
//  Created by Pavel Aristov on 15.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit
import WebKit

class BrowserController: UIViewController {

    private var url: URL!
    
    private var previousToolBarItem: UIBarButtonItem!
    private var nextToolBarItem: UIBarButtonItem!
    
    private var webView: WKWebView = {
        let webView = WKWebView()
        
//        webView.allowsBackForwardNavigationGestures = true
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        return webView
    }()
    private var progressView: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }()
    
    private var observations = [NSKeyValueObservation?]()
    
    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setToolbarHidden(false, animated: true)
        
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.navigationBar.alpha = 1
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setToolbarHidden(true, animated: true)
    }
    
    @objc private func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Add UI Elements

extension BrowserController {
    private func setupUI() {
        
        self.view.backgroundColor = .white
        
        addWebView()
        setupToolBar()
        setupNavigationBar()
    }
    
    private func addWebView() {
        
        addObservations()
        
        self.view.addSubview(webView)
        self.view.addSubview(progressView)
        
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            webView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
            webView.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            progressView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor)
        ])
    }
    
    private func updateProgress() {
        
        let progress = Float(self.webView.estimatedProgress)
        
        UIView.animate(withDuration: 0.2) {
            self.progressView.alpha = progress == 0 || progress == 1 ? 0 : 1
        }
        
        self.progressView.setProgress(progress, animated: true)
        
        self.previousToolBarItem.isEnabled = webView.canGoBack
        self.nextToolBarItem.isEnabled = webView.canGoForward
    }
    
    private func addObservations() {
        let progressObservation = webView.observe(\.estimatedProgress, options: [.new]) { _, _ in
            self.updateProgress()
        }
        
        let titleObservation = webView.observe(\.title, options: [.new]) { _, _ in
            self.title = self.webView.url?.host //title
        }
        
        observations = [progressObservation, titleObservation]
    }
    
    private func setupNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backIcon"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "shareIcon"), style: .plain, target: self, action: #selector(shareButtonAction))
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    private func setupToolBar() {
        
        self.navigationController?.toolbar.tintColor = Pallete.Common.NavigationBar.tintColor
        
        self.previousToolBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "browserBack"), style: .plain, target: self, action: #selector(previousPageAction))
        self.previousToolBarItem.imageInsets.left = 5
        self.previousToolBarItem.imageInsets.right = 5
        
        self.nextToolBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "browserNext"), style: .plain, target: self, action: #selector(nextPageAction))
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let openInSafari = UIBarButtonItem(image: #imageLiteral(resourceName: "browserIcon2"), style: .plain, target: self, action: #selector(openInSafariAction))
        openInSafari.imageInsets.left = 5
        openInSafari.imageInsets.right = 5
        
        toolbarItems = [previousToolBarItem, space, nextToolBarItem, space, space, openInSafari]
    }
}

extension BrowserController: WKNavigationDelegate {
    
    private func loadRequest() {
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    @objc private func shareButtonAction() {
        
        let textToShare = [url]
        
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc private func openInSafariAction() {
        guard let url = webView.url else { return }
        openLink(url)
    }
    
    @objc private func previousPageAction() {
        webView.goBack()
    }
    
    @objc private func nextPageAction() {
        webView.goForward()
    }
}
