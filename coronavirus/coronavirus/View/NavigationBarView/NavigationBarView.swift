//
//  NavigationBarView.swift
//  coronavirus
//
//  Created by Pavel Aristov on 10.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import UIKit

@IBDesignable
class NavigationBarView: NibView {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    
    @IBInspectable var title: String {
        get {
            return _title ?? "Title"
        }
        set {
            _title = newValue
        }
    }
    
    @IBInspectable var subtitle: String? {
        get {
            return _subtitle
        }
        set {
            guard let newValue = newValue, !newValue.isEmpty else {
                _subtitle = currentDateSubtitle
                return
            }
            
            _subtitle = newValue
        }
    }
    
    private var _title: String! {
        didSet { updateLabels() }
    }
    private var _subtitle: String? {
        didSet { updateLabels() }
    }
    
    private var currentDateSubtitle: String {
        let formatter = DateFormatter()
        formatter.locale = Global.locale
        formatter.dateFormat = "dd MMMM, yyyy"
        return formatter.string(from: Date())
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateLabels()
    }
    
    convenience init(title: String, subtitle: String? = nil) {
        self.init()
        
        self.title = title
        self.subtitle = subtitle
    }
    
    private func updateLabels() {
        guard titleLabel != nil && subtitleLabel != nil else { return }
        
        titleLabel.text = title
        subtitleLabel.text = subtitle ?? currentDateSubtitle
    }
}
