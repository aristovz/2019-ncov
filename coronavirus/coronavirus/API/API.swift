//
//  API.swift
//  coronavirus
//
//  Created by Pavel Aristov on 05.02.2020.
//  Copyright © 2020 aristovz. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    static func getNews(completion: @escaping ([News], Error?) -> Void) {
        db.collection("ru").getDocuments { snapshot, error in
            
            guard error == nil else {
                print(error!.localizedDescription)
                completion([], error)
                return
            }
            
            guard let snapshot = snapshot else {
                print("Can't get data from snapshot in \(#function)")
                completion([], error)
                return
            }
            
            let news = snapshot.documents.compactMap ({
                try? JSONDecoder().decode(News.self, withJSONObject: $0.data())
            })
            
            completion(news, nil)
        }
    }
    
    static func getCountriesStats(completion: @escaping ([BaseCity], String?) -> Void) {
        Alamofire.request("https://coronavirus-monitor.ru/api/v1/statistics/get-countries").responseJSON { response in
            getResponse(from: response) { content in
                
                guard let countriesStatsMap = content["countries"] as? [[String: Any]] else {
                    completion([], "[API] Can't get `countriesStatsMap` in \(#function)")
                    return
                }
                
                let countriesStats = countriesStatsMap.compactMap {
                    BaseCity.baseMap(from: $0)
                }
                
                Global.countriesStatistics = countriesStats
                
                completion(countriesStats, nil)
            }
        }
    }
    
    static func updateStatistics(completion: @escaping (OverallStatistics?, String?) -> Void) {
        
        Alamofire.request("https://coronavirus-monitor.ru/api/v1/statistics/get-cities").responseJSON { response in
            getResponse(from: response) { content in
                
                guard let overallStatistics = OverallStatistics.map(from: content) else {
                    completion(nil, "[API] Error parsing response")
                    return
                }
                
                Global.overallStatistics = overallStatistics
                completion(overallStatistics, nil)
            }
        }
    }
    
}

extension API {
    private static func getResponse(from response: DataResponse<Any>, completion: @escaping ([String: Any]) -> Void) {
            
        let statusCode = response.response?.statusCode
        
        guard [200, 201, 202].contains(statusCode) else {
            print("[API] Bad status code: \(statusCode ?? -1)")
            let errorContent = response.result.value as? [String: Any] ?? [:]
            completion(["error": "Bad status code", "errorContent": errorContent])
            return
        }
        
        guard let content = response.result.value as? [String: Any] else {
            print("[API] Error: Can't get value in \(#function)")
            completion([:])
            return
        }
            
        guard let data = content["data"] as? [String: Any] else {
            print("[API] Error: Can't get data in \(#function)")
            completion([:])
            return
        }
        
        completion(data)
    }
}
