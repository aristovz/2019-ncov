//
//  NotificationsService.swift
//  BlockchainApp
//
//  Created by Pavel Aristov on 30.10.2019.
//  Copyright © 2019 aristovz. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications

import Firebase
import FirebaseMessaging


class NotificationsService {
    
    private init() { }
    
    static let shared = NotificationsService()
 
    func registerForPushNotifications(force: Bool = false, _ completion: ((Bool) -> Void)? = nil) {

        guard !force else {
            requestAuthorization(completion)
            return
        }
        
        checkAuthorizationStatus { isAuthorized in
            guard !isAuthorized else { return }
            
            self.requestAuthorization(completion)
        }
    }
    
    func unregisterForPushNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    private func requestAuthorization(_ completion: ((Bool) -> Void)? = nil) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            print("[NotificationsService] Permission granted: \(granted)")

            DispatchQueue.main.async {
                completion?(granted)
            }
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func checkAuthorizationStatus(completion: @escaping (Bool) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            DispatchQueue.main.async {
                completion(settings.authorizationStatus == .authorized)
            }
        }
    }
    
    func getNotificationSettings(completion: ((UNNotificationSettings) -> Void)? = nil) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//            print("[NotificationsService] Notification settings: \(settings)")
            completion?(settings)
            
            guard settings.authorizationStatus == .authorized else { return }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
}

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
          return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("[NotificationsService] Device Token: \(token)")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("[NotificationsService] Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("[NotificationsService] Remote instance ID token: \(result.token)")
                Global.pushToken = token
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("[NotificationsService] Failed to register: \(error)")
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("[NotificationsService.MessagingDelegate] Registration Token: \(fcmToken)")
        Global.fcmPushToken = fcmToken
    }
}
